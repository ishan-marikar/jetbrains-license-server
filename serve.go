package license

import (
	"encoding/xml"
	"fmt"
	"net/http"
	"net/url"
)

func NewMux() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/rpc/ping.action", handle(handlePing))
	mux.HandleFunc("/rpc/obtainTicket.action", handle(handleObtainTicket))
	return mux
}

func handle(callback func(url.Values) interface{}) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			rw.WriteHeader(http.StatusServiceUnavailable)
			return

		}
		payload, _ := xml.MarshalIndent(
			callback(r.URL.Query()),
			"",
			"  ",
		)
		signed, err := signature(payload)

		response := "<!-- " + signed + " -->\n" + string(payload)

		fmt.Println()
		fmt.Println(r.URL)
		fmt.Println(response)

		if err != nil {
			rw.WriteHeader(http.StatusServiceUnavailable)
			rw.Write([]byte(err.Error()))
			return
		}
		rw.Header().Set("Content-Type", "text/xml")
		rw.Write([]byte(response))
	}
}

func handlePing(query url.Values) interface{} {
	return &PingResponse{
		Message:      query.Get("salt"),
		ResponseCode: "OK",
	}

}

func handleObtainTicket(query url.Values) interface{} {
	ticketProperties := joinTicketProperties(map[string]string{
		"licensee":    query.Get("userName"),
		"licenseType": "0",
	})
	return &ObtainTicketResponse{
		ProlongationPeriod: 0x243B71AC,
		ResponseCode:       "OK",
		Salt:               query.Get("salt"),
		TicketID:           1,
		TicketProperties:   ticketProperties,
	}
}
