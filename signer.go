package license

import (
	"crypto"
	"crypto/md5"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
)

var (
	defaultPrivateKeyPEM = []byte(`
-----BEGIN RSA PRIVATE KEY-----
MIIBOwIBAAJBALecq3BwAI4YJZwhJ+snnDFj3lF3DMqNPorV6y5ZKXCiCMqj8OeO
mxk4YZW9aaV9ckl/zlAOI0mpB3pDT+Xlj2sCAwEAAQJAW6/aVD05qbsZHMvZuS2A
a5FpNNj0BDlf38hOtkhDzz/hkYb+EBYLLvldhgsD0OvRNy8yhz7EjaUqLCB0juIN
4QIhAMsJQ3xiJemnJ2pD65iRNCC/Kr7jtxbbBwa6ZFLjp12pAiEA54JCn41fF8GZ
90b9L5dtFQB2/yIcGX4Xo7bCvl8DaPMCIBgOZ+2T33QYtwXTOFXiVm/O1qy5ZFcT
6ng0m3BqwsjJAiEAqna/l7wAyP1E4U7kHqbhKxWsiTAUgLDXtzRbMNHFMQECIQCA
xlpXEPqnC3P8if0G9xHomqJ531rOJuzB8fNtRFmxnA==
-----END RSA PRIVATE KEY-----
`)
)

func getPrivateKey() (*rsa.PrivateKey, error) {
	block, _ := pem.Decode([]byte(defaultPrivateKeyPEM))
	return x509.ParsePKCS1PrivateKey(block.Bytes)
}

func signature(payload []byte) (string, error) {
	key, err := getPrivateKey()
	if err != nil {
		return "", err
	}
	hashed := md5.Sum(payload)
	data, err := key.Sign(rand.Reader, hashed[:], crypto.MD5)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(data), nil
}
